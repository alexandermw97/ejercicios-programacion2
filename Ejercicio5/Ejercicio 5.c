/* Ejercicio 5: Triangulo isosceles */

#include <stdio.h>
#include <stdlib.h>

int main()
{
  float perimetro,longitud1, longitud2;

  //Tri�ngulo rect�ngulo

  printf("Perimetro de un triangulo Isosceles\n");
  printf("\nDame la longitud1: ");
  scanf("%f",&longitud1);
  printf("\nDame la longitud2: ");
  scanf("%f",&longitud2);
  perimetro=(longitud1*2)+longitud2;
  printf("\nEl perimetro del triangulo equilatero es: %.3f\n ",perimetro);

      system("PAUSE");
      return 0;
}
