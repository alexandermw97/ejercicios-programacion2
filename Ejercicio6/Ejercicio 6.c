/* Ejercicio 6: Triangulo Escaleno */

#include <stdio.h>
#include <stdlib.h>

int main()
{
  float perimetro,longitud1, longitud2,longitud3;

  //Tri�ngulo rect�ngulo

  printf("Perimetro de un triangulo Escaleno\n");
  printf("\nDame la longitud1: ");
  scanf("%f",&longitud1);
  printf("\nDame la longitud2: ");
  scanf("%f",&longitud2);
  printf("\nDame la longitud3: ");
  scanf("%f",&longitud3);
  perimetro=longitud1+longitud2+longitud3;
  printf("\nEl perimetro del triangulo equilatero es: %.3f\n ",perimetro);

  system("PAUSE");
  return 0;
}
